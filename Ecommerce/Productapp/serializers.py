from rest_framework import serializers
from .models import category,Product,subcategory

class subcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = subcategory
        fields ='__all__'

class productSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields ='__all__'

class categorySerializer(serializers.ModelSerializer):
    class Meta:
        model = category
        fields ='__all__'

