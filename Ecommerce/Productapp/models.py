from django.db import models
from Vendorapp.models import Vendor


class category(models.Model):
    category_name=models.CharField(max_length=50,null=True)
    category_image=models.ImageField(upload_to='prod',null=True,blank=True)
    category_desc=models.CharField(max_length=20,null=True)
    Vendor1=models.ForeignKey(Vendor,on_delete=models.CASCADE,null=True)

    def __str__(self):
        return self.category_name

class subcategory(models.Model):
    subcategory_name=models.CharField(max_length=50,null=True)
    subcategory_image=models.ImageField(upload_to='subcat',null=True,blank=True)
    subcategory_desc=models.CharField(max_length=50,null=True)
    category_id=models.ForeignKey(category,on_delete=models.CASCADE,null=True)
    Vendor_id=models.ForeignKey(Vendor,on_delete=models.CASCADE,null=True)

    def __str__(self):
      return self.subcategory_name

class Product(models.Model):
     product_name=models.CharField(max_length=100,null=True)
     product_image1=models.ImageField(upload_to='productfile/',null=True,blank=True)
     product_image2=models.ImageField(upload_to='productfile/',null=True,blank=True)
     product_image3=models.ImageField(upload_to='productfile/',null=True,blank=True)
     product_desc=models.CharField(max_length=20,null=True)
     price=models.FloatField(null=True)
     stock=models.IntegerField(null=True)
     stock_status=models.CharField(max_length=50,null=True)
     subcategory=models.ForeignKey(subcategory,on_delete=models.CASCADE,null=True)
     Vendorr=models.ForeignKey(Vendor,on_delete=models.CASCADE,null=True,blank=True)
     categoryy=models.ForeignKey(category,on_delete=models.CASCADE,null=True,blank=True)

     def __str__(self):
         return self.product_name