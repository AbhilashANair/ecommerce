from django.shortcuts import render,redirect
from .models import category,subcategory,Product
from Clientapp.models import *
from Vendorapp.models import *
from .serializers import categorySerializer,subcategorySerializer,productSerializer
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from rest_framework.response import Response
from rest_framework.decorators import api_view


def add_prod(request):
    if request.method == 'POST':
        pname = request.POST.get('pname')
        p_image = request.POST.get('pimage')
        p_desc = request.POST.get('pdesc')
        p_price = request.POST.get('pprice')
        p_stock = request.POST.get('pstock')
        p_sstatus=request.POST.get('psstatus') 
        p_cat = request.POST.get('pcat')
        cfk=category.objects.get(category_name=p_cat)
        p_subcat = request.POST.get('pscat')
        r=subcategory.objects.get(subcategory_name=p_subcat)
        ven_name = request.POST.get('pvendor')
        vfk=Vendor.objects.get(Vendor_Name=ven_name)
       
        oa= Product(product_name=pname,product_image=p_image,product_desc=p_desc,price=p_price,stock=p_stock,stock_status=p_sstatus,subcategory=r,Vendorr=vfk,categoryy=cfk)
        oa.save()
        return redirect(show_prod) 
    cat=category.objects.all()
    scat=subcategory.objects.all()
    vendor=Vendor.objects.all()
    return render(request,"add_prod.html",{'cat':cat,'subcat':scat,'vendor':vendor}) 

def searchorder(request):
    rp=orderproducts.objects.all()
    if request.method == 'POST':
        deliverydate=request.POST.get('deliverydate')
        vendorname=request.POST.get('vendorname')
        productname=request.POST.get('productname')
        customeremail=request.POST.get('customeremail')
        if deliverydate:
            k1=orderproducts.objects.filter(deliverydate=deliverydate)
        elif vendorname:
            k1=orderproducts.objects.filter(vendordetails__vend_name=vendorname)
        elif productname:
            k1=orderproducts.objects.filter(productdetails__product_name=productname)
        elif customeremail:
            k1=orderproducts.objects.filter(customerdetails__custemail=customeremail)
        else:
            k1=orderproducts.objects.filter(deliverydate=deliverydate,vendordetails__vend_name=vendorname,productdetails__product_name=productname,customerdetails__custemail=customeremail)
    return render(request,"ordersearch.html",{'k1':k1})     
    

def updateprod(request,productid):
    k=Product.objects.filter(id=productid).values()
    if request.method == 'POST':
        pname = request.POST.get('pname')
        p_image = request.POST.get('pimage')
        p_desc = request.POST.get('pdesc')
        p_price = request.POST.get('pprice')
        p_stock = request.POST.get('pstock')
        p_sstatus=request.POST.get('psstatus') 
        p_cat = request.POST.get('pcat')
        cfk=category.objects.get(category_name=p_cat)
        p_subcat = request.POST.get('pscat')
        r=subcategory.objects.get(subcategory_name=p_subcat)
        ven_name = request.POST.get('pvendor')
        vfk=Vendor.objects.get(Vendor_Name=ven_name)
        k.update(product_name=pname,product_image=p_image,product_desc=p_desc,price=p_price,stock=p_stock,stock_status=p_sstatus,subcategory=r,Vendorr=vfk,categoryy=cfk)
        return redirect('showprod')
    cat=category.objects.all()
    scat=subcategory.objects.all()
    vendor=Vendor.objects.all()
    return render(request,"update_prod.html",{'k':k[0],'id':productid,'cat':cat,'subcat':scat,'vendor':vendor}) 
          
 
def deleteprod(request,productid):
   k=Product.objects.get(id=productid)
   k.delete()
   return redirect("showprod")   

def show_prod(request):
    lc=Product.objects.all()
    return render(request,"productlist.html",{'lc':lc})

def add_cat(request):
    if request.method == 'POST':
        cname = request.POST.get('cname')
        c_image = request.POST.get('cimage')
        c_desc = request.POST.get('cdesc')
        ven_name = request.POST.get('cvendor')
        vfk=Vendor.objects.get(Vendor_Name=ven_name)
        oa= category(category_name=cname,category_image=c_image,category_desc=c_desc,Vendor1=vfk)
        oa.save()
        return redirect(show_cat) 
    vendor=Vendor.objects.all()
    return render(request,"addcat.html",{'vendor':vendor}) 


def updatecat(request,categoryid):
    k=category.objects.filter(id=categoryid).values()
    if request.method == 'POST':
        cname = request.POST.get('cname')
        c_image = request.POST.get('cimage')
        c_desc = request.POST.get('cdesc')
        ven_name = request.POST.get('cvendor')
        vfk=Vendor.objects.get(Vendor_Name=ven_name)
        k.update(category_name=cname,category_image=c_image,category_desc=c_desc,Vendor1=vfk)
        return redirect(show_cat)
    vendor=Vendor.objects.all()
    return render(request,"update_cat.html",{'k':k[0],'id':categoryid,'vendor':vendor}) 
          
 
def deletecat(request,categoryid):
   k=category.objects.get(id=categoryid)
   k.delete()
   return redirect("showcat")   

def show_cat(request):
    lc=category.objects.all()
    return render(request,"categorylist.html",{'lc':lc})    


def add_sc(request):
    if request.method == 'POST':
        scname = request.POST.get('scname')
        sc_image = request.POST.get('scimage')
        sc_desc = request.POST.get('scdesc')
        scat_name = request.POST.get('catname')
        cfk=category.objects.get(category_name=scat_name)
        ven_name = request.POST.get('vendor')
        vfk=Vendor.objects.get(Vendor_Name=ven_name)
        oa= subcategory(subcategory_name=scname,subcategory_image=sc_image,subcategory_desc=sc_desc,category_id=cfk,Vendor_id=vfk)
        oa.save()
        return redirect(show_sc) 
    vendor=Vendor.objects.all()
    cat=category.objects.all()
    return render(request,"add_sc.html",{'vendor':vendor,'cat':cat}) 


def updatesc(request,subcategoryid):
    k=subcategory.objects.filter(id=subcategoryid).values()
    if request.method == 'POST':
        scname = request.POST.get('scname')
        sc_image = request.POST.get('scimage')
        sc_desc = request.POST.get('scdesc')
        scat_name = request.POST.get('catname')
        cfk=category.objects.get(category_name=scat_name)
        ven_name = request.POST.get('vendor')
        vfk=Vendor.objects.get(Vendor_Name=ven_name)
        k.update(subcategory_name=scname,subcategory_image=sc_image,subcategory_desc=sc_desc,category_id=cfk,Vendor_id=vfk)
        return redirect(show_sc)
    vendor=Vendor.objects.all()
    cat=category.objects.all()
    return render(request,"update_sc.html",{'k':k[0],'id':subcategoryid,'vendor':vendor,'cat':cat}) 
          
 
def deletesc(request,subcategoryid):
   k=subcategory.objects.get(id=subcategoryid)
   k.delete()
   return redirect(show_sc)   

def show_sc(request):
    lc=subcategory.objects.all()
    return render(request,"subcatlist.html",{'lc':lc})    
