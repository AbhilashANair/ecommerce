"""Ecommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from Productapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
         #PRODUCTS
    path('add_prod',views.add_prod,name='addprod'),
    path('show_prod',views.show_prod,name='showprod'),
    path('delete_prod/<int:productid>',views.deleteprod,name='del_prod'),
    path('update_prod/<int:productid>',views.updateprod,name='up_prod'),
           #CATEGORY
    path('add_cat',views.add_cat,name='addcat'),
    path('show_cat',views.show_cat,name='showcat'),
    path('delete_cat/<int:categoryid>',views.deletecat,name='del_cat'),
    path('update_cat/<int:categoryid>',views.updatecat,name='up_cat'),       
           #SUB_CATEGORY
    path('add_sc',views.add_sc,name='addsc'),
    path('show_sc',views.show_sc,name='showsc'),
    path('delete_sc/<int:subcategoryid>',views.deletesc,name='del_sc'),
    path('update_sc/<int:subcategoryid>',views.updatesc,name='up_sc'),       

    
    ]