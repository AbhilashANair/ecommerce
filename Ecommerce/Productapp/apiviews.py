from django.shortcuts import render,redirect
from .models import *
from .serializers import categorySerializer,subcategorySerializer,productSerializer
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from rest_framework.response import Response
from rest_framework.decorators import api_view
from Vendorapp.models import Vendor
from Clientapp.models import Orders,Order_Details


@api_view(['POST'])
def add_cat(request):
    if request.method=='POST':
        cat_name=request.data.get('category_name')
        cat_image=request.data.get('category_image')
        cat_desc=request.data.get('category_desc')
        Vname=request.data.get('Vendor1')
        vendorname=request.objects.get(Vendor_Name=Vname)
        category.objects.create(category_name=cat_name,category_image=cat_image,category_desc=cat_desc,Vendor1=vendorname)
        return Response({'message':'addcat success'})

@api_view(['GET'])
def show_cat(request): 
    if request.method=='POST':
        scat=category.objects.all()
        Serializer=categorySerializer(scat,many=TRUE)
        return Response(Serializer.data)

@api_view(['POST'])
def add_subcat(request):
    if request.method=='POST':
        subcat_name=request.data.get('subcategory_name')
        subcat_image=request.data.get('subcategory_image')
        subcat_desc=request.data.get('subcategory_desc')
        catname=request.data.get('category_id')
        cname=request.object.get(category_name=catname)
        venname=request.data.get('Vendor_id')
        vname=request.object.get(Vendor_Name=venname)
        subcategory.objects.create(subcategory_name=,subcategory_image=,subcategory_desc=,category_id=,Vendor_id=)
        return Response({'message':'addsubcat success'})

@api_view(['GET'])
def show_subcat(request): 
    if request.method=='POST':
        subcat=category.objects.all()
        Serializer=subcategorySerializer(subcat,many=TRUE)
        return Response(Serializer.data)


@api_view(['POST'])
def add_prod(request):
    if request.method=='POST':
        prodname=request.data.get('product_name')
        prodimage=request.data.get('product_image')
        proddesc=request.data.get('product_desc')
        pprice=request.data.get('price')
        pstock=request.data.get('stock')
        stockstatus=request.data.get('stock_status')
        scat=request.data.get('subcategory')
        subc=request.object,get(subcategory_name=scat)
        vName=request.data.get('Vendorr')
        vname=request.object.get(Vendor_Name=vName)
        cat=request.data.get('categoryy')
        cname=request.object.get(category_name=cat)

        Product.objects.create(product_name=prodname,product_image=prodimage,product_desc=proddesc,price=pprice,stock=pstock,stock_status=stockstatus,subcategory=subc,Vendorr=vname,categoryy=cname)
        return Response ({'message':'addprod success'})

@api_view(['GET'])
def show_prod(request): 
    if request.method=='POST':
        prod=Product.objects.all()
        Serializer=productSerializer(prod,many=TRUE)
        return Response(Serializer.data)        

       





































































































 
# @api_view(['POST'])
# def addproduct(request):
#     if request.method == 'POST':
#         pro_name=request.data.get('product_name')
#         pro_img1=request.data.get('product_image1')
#         pro_img2=request.data.get('product_image2')
#         pro_img3=request.data.get('product_image3')
#         pro_desc=request.data.get('product_description')
#         pro_price=request.data.get('price')
#         pro_stock=request.data.get('stock')
#         cat_id=request.data.get('category_id')
#         sub_id=request.data.get('subcategory_id')
#         sto_status=request.data.get('stock_status')
#         ven_id=request.data.get('vend_id')
#         catdet=category.objects.get(category_id=cat_id)
#         subcatdet=subcategory.objects.get(subcategory_id=sub_id)
#         vendet=vendor.objects.get(vendor_id=ven_id)
#         ap=product.objects.create(product_name=pro_name,product_image1=pro_img1,product_image2=pro_img2,product_image3=pro_img3,product_description=pro_desc,price=pro_price,stock=pro_stock,stock_status=sto_status,category_id=catdet,subcategory_id=subcatdet,vend_id=vendet)
#         ap.save()
#         proadd = product.objects.all()
#         serializer = productSerializer(proadd,many=True)
#         return Response(serializer.data)
#     return Response("Product Added Sucessfully")

# @api_view()
# def showproduct(request):
#     prod = product.objects.all()
#     serializer = productSerializer(prod,many=True)
#     return Response(serializer.data)

# @api_view(['POST'])
# def addcategory(request):
#     if request.method == 'POST':
#         cat_name=request.data.get('category_name')
#         cat_image=request.data.get('category_image')
#         cat_desc=request.data.get('category_description')
#         vend_id=request.data.get('ve_id')
#         vv_id=vendor.objects.get(vendor_id=vend_id)
#         adcat=category.objects.create(category_name=cat_name,category_image=cat_image,category_description=cat_desc,ve_id=vv_id)
#         adcat.save()
#          # return Response("added")
#         cateadd = category.objects.all()
#         serializer =  categorySerializer(cateadd,many=True)
#         return Response(serializer.data)
#     return Response("category Added Sucessfully")

# @api_view(['POST'])
# def addsubcategory(request):
#     if request.method == 'POST':
#         subcat_name=request.data.get('subcategory_name')
#         subcat_image=request.data.get('subcategory_image')
#         subcat_desc=request.data.get('subcategory_description')
#         cate_id=request.data.get('category_id')
#         vend_id=request.data.get('ven_id')
#         vv_id=vendor.objects.get(vendor_id=vend_id)
#         cc_id=category.objects.get(category_id=cate_id)
#         adcat=subcategory.objects.create(subcategory_name=subcat_name,subcategory_image=subcat_image,subcategory_description=subcat_desc,category_id=cc_id,ven_id=vv_id)
#         adcat.save()
#         subcateadd = subcategory.objects.all()
#         serializer = subcategorySerializer(subcateadd,many=True)
#         return Response(serializer.data)
#     return Response("subcategory Added Sucessfully")



