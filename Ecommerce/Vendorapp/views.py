from django.shortcuts import render,redirect
from .models import *
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage


def add_ven(request):
    if request.method == 'POST':
        vname = request.POST.get('vname')
        vemail = request.POST.get('vemail')
        vbranch = request.POST.get('vbranch')
        vurl = request.POST.get('vurl')
        vaddress = request.POST.get('vaddress')
        vphone = request.POST.get('vphone')
        vgst = request.POST.get('vgst')
        oa= Vendor(Vendor_Name=vname,Vendor_Email=vemail,Vendor_branch=vbranch,Vendor_URL=vurl,Vendor_Address=vaddress,Vendor_Phone=vphone,Vendor_GST=vgst)
        oa.save()
        return redirect(show_ven) 
    return render(request,"add_ven.html") 


def updateven(request,vendorid):
    k=Vendor.objects.filter(id=vendorid).values()
    if request.method == 'POST':
        vname = request.POST.get('vname')
        vemail = request.POST.get('vemail')
        vbranch = request.POST.get('vbranch')
        vurl = request.POST.get('vurl')
        vaddress = request.POST.get('vaddress')
        vphone = request.POST.get('vphone')
        vgst = request.POST.get('vgst')
        k.update(Vendor_Name=vname,Vendor_Email=vemail,Vendor_branch=vbranch,Vendor_URL=vurl,Vendor_Address=vaddress,Vendor_Phone=vphone,Vendor_GST=vgst)
        return redirect(show_ven)
    return render(request,"update_ven.html",{'k':k[0],'id':vendorid}) 
          
 
def deleteven(request,vendorid):
   k=Vendor.objects.get(id=vendorid)
   k.delete()
   return redirect(show_ven)   

def show_ven(request):
    lc=Vendor.objects.all()
    return render(request,"vendorlist.html",{'lc':lc})    
