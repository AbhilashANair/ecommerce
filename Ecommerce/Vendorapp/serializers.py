from rest_framework import serializers
from Vendorapp.models import Vendor


class vendorserializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields ='__all__'

