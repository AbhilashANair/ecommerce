from django.shortcuts import render,redirect
from .models import *
from .serializers import vendorserializer
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from rest_framework.response import Response
from rest_framework.decorators import api_view


@api_view(['POST'])
def addvendor(request):
    if request.method == 'POST':
        ven_email=request.data.get('Vendor_Email')
        ven_branch=request.data.get('Vendor_branch')
        ven_name=request.data.get('Vendor_Name')
        ven_url=request.data.get('Vendor_URL')
        ven_password=request.data.get('Vendor_Password')
        ven_address=request.data.get('Vendor_Address')
        ven_phone=request.data.get('Vendor_Phone')
        ven_gst=request.data.get('Vendor_GST')
        vendor.objects.create(Vendor_Email=ven_email,Vendor_branch=ven_branch,Vendor_Name=ven_name,Vendor_URL=ven_url,Vendor_Password=ven_password,Vendor_Address=ven_address,Vendor_Phone=ven_phone,Vendor_GST=ven_gst)
        return Response("Vendor Added Sucessfully")


@api_view()
def showvendor(request):
    pro = Vendor.objects.all()
    serializer =vendorserializer(pro,many=True)
    return Response(serializer.data)

# @api_view(['POST'])
# def updatevendor(request,userid):
#     u=vendor.objects.filter(id=userid)
#     if request.method == 'POST':
#         ven_id=request.data.get('vendor_id')
#         ven_email=request.data.get('vendor_email')
#         ven_branch=request.data.get('vendor_branch')
#         ven_name=request.data.get('vendor_name')
#         ven_url=request.data.get('vendor_URL')
#         ven_password=request.data.get('vendor_password')
#         ven_address=request.data.get('vendor_address')
#         ven_phone=request.data.get('vendor_phone')
#         ven_gst=request.data.get('vendor_GST')
#         av=vendor.objects.create(vendor_id=ven_id,vendor_email=ven_email,vendor_branch=ven_branch,vendor_name=ven_name,vendor_URL=ven_url,vendor_password=ven_password,vendor_address=ven_address,vendor_phone=ven_phone,vendor_GST=ven_gst)
#         av.save()
#         venadd = vendor.objects.all()
#         serializer = vendorserializer(venadd,many=True)
#         return Response(serializer.data)

# @api_view(['GET'])
# def deletevendor(request,userid):
#     sd=vendor.objects.filter(id=userid)
#     sd.delete()
#     return Response("vendor Removed")