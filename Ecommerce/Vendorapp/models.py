from django.db import models

class Vendor(models.Model): 
    
    Vendor_Email=models.EmailField(max_length=50,null=True)
    Vendor_branch=models.CharField(max_length=30,null=True)
    Vendor_Name=models.CharField(max_length=40,null=True)
    Vendor_URL=models.CharField(max_length=80,null=True)
    # Vendor_Password=models.CharField(max_length=70,null=True)
    Vendor_Address=models.CharField(max_length=80,null=True)
    Vendor_Phone=models.IntegerField(null=True)
    Vendor_GST=models.IntegerField(null=True)

    def __str__(self):
        return self.Vendor_Email



