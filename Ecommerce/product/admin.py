from django.contrib import admin
from .models import productmodel,category,subcategory
# Register your models here.

admin.site.register(productmodel)
admin.site.register(category)
admin.site.register(subcategory)