from django.shortcuts import render,redirect
from rest_framework.response import Response
from .models import productmodel,category,subcategory
from vendor.models import vendor
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.views import APIView
from django.core.files.storage import FileSystemStorage
from .serializers import Productserializer,Categoryserializer,Subcategoryserializer

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def addproduct(request):
    if request.method == 'POST':
        name = request.data.get('product_name')
        # image1 = request.data.get['image1']
        # i1 = FileSystemStorage()
        # filename1 = i1.save(image1.name,image1)
        # image2 = request.data.get['image2']
        # i2 = FileSystemStorage()
        # filename2 = i2.save(image2.name,image2)
        # image3 = request.data.get['image3']
        # i3 = FileSystemStorage()
        # filename3 = i3.save(image3.name,image3)
        productdesc = request.data.get('product_description')
        price = request.data.get('price')
        stock = request.data.get('stock')
        categ = request.data.get('category_name')
        c = category.objects.get(category_name=categ)
        subcateg = request.data.get('subcategory_name')
        sc = subcategory.objects.get(subcat_name=subcateg)
        vendoremail = request.data.get('vendor_email')
        vc = vendor.objects.get(vendor_email=vendoremail)

        productmodel.objects.create(product_name=name, product_description=productdesc,price=price,stock=stock,category_name=c,subcategory_name=sc,vendor_name=vc)

        pm = productmodel.objects.all()
        serialr = Productserializer(pm, many=True)
        return Response(serialr.data)
    return Response("Added Product")

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def addcategory(request):
    if request.method == 'POST':
        categname = request.data.get('category_name')
        # categimage = request.data.get('category_image')
        # categimage = request.FILES['category_image']
        # i3 = FileSystemStorage()
        # filecateg = i3.save(categimage.name, categimage)
        categdesc = request.data.get('category_description')
        vendoremail = request.data.get('vendor_email')
        vc = vendor.objects.get(vendor_email=vendoremail)

        c=category(category_name=categname, category_description=categdesc, vendor_name=vc)
        c.save()
        cs = category.objects.all()
        serialc = Categoryserializer(cs, many=True)
        return Response(serialc.data)
    return Response("Added category")

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def addsubcategory(request):
    if request.method == 'POST':
        subcategname = request.data.get('subcat_name')
        # subcategimage = request.data.get('subcat_image')
        # subcategimage = request.FILES['subcat_image']
        # i3 = FileSystemStorage()
        # filesubcateg = i3.save(subcategimage.name, subcategimage)
        subcategdesc = request.data.get('subcat_description')
        categname = request.data.get('category_name')
        c = category.objects.get(category_name=categname)
        vendoremail = request.data.get('vendor_email')
        vc = vendor.objects.get(vendor_email=vendoremail)

        subcategory.objects.create(subcat_name=subcategname, subcat_description=subcategdesc, category_name=c, vendor_name=vc)

        scs = subcategory.objects.all()
        serialsc = Subcategoryserializer(scs, many=True)
        return Response(serialsc.data)
        
    return Response("Added Subcategory")

@api_view()
@permission_classes([IsAuthenticated])
def showallproducts(request):
    ad = productmodel.objects.all()
    serialr = Productserializer(ad, many=True)
    return Response(serialr.data)

@api_view()
@permission_classes([IsAuthenticated])
def showallcategory(request):
    sc = category.objects.all()
    serialc = Categoryserializer(sc, many=True)
    return Response(serialc.data)

@api_view()
@permission_classes([IsAuthenticated])
def deleteapiproduct(request,productid):
    delp = productmodel.object.values(id=productid)
    delp.delete()

    p = productmodel.objects.all()
    serialp = Productserializer(p, many=True)
    return Response(serialp.data)