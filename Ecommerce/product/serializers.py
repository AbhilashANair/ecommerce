from rest_framework import serializers
from .models import productmodel,category,subcategory

class Productserializer(serializers.ModelSerializer):
    class Meta:
        model = productmodel
        fields = '__all__'

class Categoryserializer(serializers.ModelSerializer):
    p = Productserializer(source='productmodel_set', many=True)
    class Meta:
        model = category
        fields = ['category_name', 'category_description','p']

class Subcategoryserializer(serializers.ModelSerializer):
    cat = Categoryserializer(source='category_set', many=True)
    class Meta:
        model = subcategory
        fields = ['subcat_name','subcat_description','cat']

