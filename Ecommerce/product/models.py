from django.db import models
from vendor.models import vendor
# Create your models here.

class category(models.Model):
    category_name = models.CharField(max_length=50, null=True)
    category_image = models.ImageField(upload_to='categoryfile/', null=True, blank=True)
    category_description = models.CharField(max_length=200, null=True)
    vendor_email = models.ForeignKey(vendor, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.category_name

class subcategory(models.Model):
    subcat_name = models.CharField(max_length=50, null=True)
    subcat_image = models.ImageField(upload_to='subcategoryfile/', null=True, blank= True)
    subcat_description = models.CharField(max_length=200, null=True)
    category_name = models.ForeignKey(category, on_delete=models.CASCADE, null=True)
    vendor_email = models.ForeignKey(vendor, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.subcat_name

class productmodel(models.Model):
    product_name = models.CharField(max_length=50, null=True)
    image1 = models.ImageField(upload_to='productfile/', null=True, blank=True)
    image2 = models.ImageField(upload_to='productfile/', null=True, blank=True)
    image3 = models.ImageField(upload_to='productfile/', null=True, blank=True)
    product_description = models.CharField(max_length=200, null=True)
    price = models.FloatField(null=True)
    stock = models.IntegerField(null=True)
    category_name = models.ForeignKey(category, on_delete=models.CASCADE, null=True)
    subcategory_name = models.ForeignKey(subcategory, on_delete=models.CASCADE, null=True)
    stock_status = models.CharField(max_length=50, null=True)
    vendor_email = models.ForeignKey(vendor, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.product_name