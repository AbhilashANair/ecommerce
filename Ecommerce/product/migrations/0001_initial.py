# Generated by Django 3.1.3 on 2021-01-11 15:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category_name', models.CharField(max_length=50, null=True)),
                ('category_image', models.ImageField(blank=True, null=True, upload_to='categoryfile/')),
                ('category_description', models.CharField(max_length=200, null=True)),
                ('vendor_name', models.CharField(max_length=50, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='subcategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subcat_name', models.CharField(max_length=50, null=True)),
                ('subcat_image', models.ImageField(blank=True, null=True, upload_to='subcategoryfile/')),
                ('subcat_description', models.CharField(max_length=200, null=True)),
                ('vendor_name', models.CharField(max_length=50, null=True)),
                ('category_name', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='product.category')),
            ],
        ),
        migrations.CreateModel(
            name='productmodel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_name', models.CharField(max_length=50, null=True)),
                ('image1', models.ImageField(blank=True, null=True, upload_to='productfile/')),
                ('image2', models.ImageField(blank=True, null=True, upload_to='productfile/')),
                ('image3', models.ImageField(blank=True, null=True, upload_to='productfile/')),
                ('product_description', models.CharField(max_length=200, null=True)),
                ('price', models.FloatField(null=True)),
                ('stock', models.IntegerField(null=True)),
                ('stock_status', models.CharField(max_length=50, null=True)),
                ('vendor_name', models.CharField(max_length=50, null=True)),
                ('category_name', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='product.category')),
                ('subcategory_name', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='product.subcategory')),
            ],
        ),
    ]
