from .models import productmodel,category
from django.forms import ModelForm

class productform(ModelForm):
    class Meta:
        model = productmodel
        fields = ['product_name','product_description','price','stock','category_name','subcategory_name','stock_status','vendor_email']

class categoryform(ModelForm):
    class Meta:
        model = category
        fields = ['category_name','category_description','vendor_email']