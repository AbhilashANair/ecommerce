"""ECOMMERCE URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from product import views,productappapi


urlpatterns = [
    path('admin/', admin.site.urls),
    path('addproduct', productappapi.addproduct, name='addproduct'),
    path('addcategory', productappapi.addcategory, name='addcategory'),
    path('addsubcategory', productappapi.addsubcategory, name='addsubcategory'),
    path('showallproduct',productappapi.showallproducts, name='showallproduct'),
    path('showallcategory',productappapi.showallcategory, name='showallcategory'),
    path('deleteapiproduct<int:productid>',productappapi.deleteapiproduct, name='deleteapiproduct'),

    path('formaddproduct',views.formaddproduct, name='formaddproduct'),
    path('viewproduct',views.viewproduct, name='viewproduct'),
    path('updateproduct<int:productid>',views.updateproduct, name='updateproduct'),
    path('deleteproduct<int:productid>',views.deleteproduct, name='deleteproduct'),
    path('category',views.addcategory, name='category'),
    path('viewcategory',views.viewcategory, name='viewcategory'),
    path('deletecategory<int:categoryid>',views.deletecategory, name='deletecategory'),
    path('subcategory',views.addsubcategory, name='subcategory'),
    path('deletesubcategory<int:subcategoryid>',views.deletesubcategory, name='deletesubcategory'),

]