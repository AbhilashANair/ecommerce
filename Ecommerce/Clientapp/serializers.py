from rest_framework import serializers
from .models import User,Order_Details,Orders


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields ='__all__'
class orderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields ='__all__'

class orderdetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order_Details
        fields ='__all__'