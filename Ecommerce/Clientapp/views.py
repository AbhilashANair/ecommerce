from django.shortcuts import render,redirect
from .models import *
# from django.contrib.auth.models import User
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import authenticate,login,logout
# -----------for basic authentication-------
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated

from .serializers import UserSerializer
from rest_framework.authtoken.views import ObtainAuthToken

def index(request):
    return render (request,"index.html")
def home(request):
    return render (request,"Home.html")
def shop(request):
    return render (request,"shop.html")
def single(request):
    return render (request,"single.html")
# def home(request):
#     return render (request,"Home.html")
# def home(request):
#     return render (request,"Home.html")

    

def Signin(request):
    if request.method=="POST":
        email=request.POST.get('Email')
        pasw=request.POST.get('Pass')
        user=authenticate(request,email=email,password=pasw)
        if user:
            login(request,user)
            return render (request,"Home.html")
        else:
            return HttpResponse(' Wrong Email And Password')
    return render(request,"login.html")          
def Signout(request):
    logout(request)
    return render(request,"login.html")
 
def signup(request):
   if request.method=='POST':
      na=request.POST.get('fName')
      la=request.POST.get('lName')
      em=request.POST.get('Email')
      ph=request.POST.get('Phone')
      ad=request.POST.get('address')    
      ci=request.POST.get('city')
      po=request.POST.get('postcode')
      un=request.POST.get('Uname')
      ps=request.POST.get('password')
      ro=request.POST.get("role")
      dob=request.POST.get("dob")
      User.objects.create_user(first_name=na,last_name=la,phone=ph,address=ad,city=ci,postcode=po,role=ro,Dob=dob,username=un,email=em,password=ps)
      return render (request,"login.html")    
   return render (request,"register.html")  


def deletecustomer(request,userid):
   k=User.objects.get(id=userid)
   k.delete()
   return redirect("listall")   

def updatecustomer(request,userid):
    k=User.objects.filter(id=userid).values()
    if request.method=='POST':
      na=request.POST.get('fName')
      la=request.POST.get('lName')
      em=request.POST.get('Email')
      ph=request.POST.get('Phone')
      ad=request.POST.get('address')    
      ci=request.POST.get('city')
      po=request.POST.get('postcode')
      un=request.POST.get('Uname')
      ro=request.POST.get("role")
      dob=request.POST.get("dob")
      k.update(first_name=na,last_name=la,phone=ph,address=ad,city=ci,postcode=po,role=ro,Dob=dob,username=un,email=em)
      return redirect('listall')
    return render (request,'update.html',{'k':k[0],'id':userid})   


def Listcustomer(request):
   d=User.objects.all()   
   return render(request,"userlist.html",{'d':d})


def searchcustomer(request):
    d=User.objects.all()
    if request.method == 'POST':
        UserName=request.POST.get('Uname')
        d=User.objects.filter(username=UserName)
        return render(request,"userlist.html",{'d':d})       
    return render(request,"userlist.html",{'d':d})
# ------------------------------------------------driver-----------------------------------------------------------
def drivers(request):
    
    if request.method == 'POST':
        name=request.POST.get('drivername')
        age=request.POST.get('driverage')
        place=request.POST.get('driverplace')
        exp=request.POST.get('experience')
        driverdetails.objects.create(driver_name=name,driver_age=age,place=place,experience=exp)
        
        
    dr=driverdetails.objects.all()
    return render(request, "driverlist.html", {'dr':dr})

def driversearch(request):
    if request.method=='POST':
        dname=request.POST.get('dname')
        dr=driverdetails.objects.filter(driver_name=dname)
    return render(request, "driverlist.html", {'dr':dr})
# ------------------------------------------------ORDER-----------------------------------------------------------

def add_order(request):
    if request.method == 'POST':
        o_num = request.POST.get('order_no')
        o_date = request.POST.get('order_date')
        total = request.POST.get('order_total')

        pname = request.POST.get('prod_name')
        r=Product.objects.get(product_name=pname)

        # price = request.POST.get('prod_price')
        quant=request.POST.get('prod_quant')
        pprice=r.price
        subtotal=int(quant)*int(pprice)
        if p.stock<=0:
            return HttpResponse("Out Of Stock")
        else:
            p.stock-=int(quant)
            p.save()
        c_email = request.POST.get('cust_email')
        cfk=User.objects.get(email=c_email)

        d_date = request.POST.get('delivery_date')
        d_status = request.POST.get('is_Delivered')
        status=request.POST.get('status')
        ven_name = request.POST.get('v_name')
        vfk=Vendor.objects.get(Vendor_Name=ven_name)

        oa= Orders(Order_no=o_num,Order_date=o_date,Order_total=total,Product_name=r,Product_Price=price,Quantity=quant,Customer_email=cfk,Delivery_date=d_date,Vendor_Name=vfk)
        oa.save()
        return redirect(show_order) 
    product=Product.objects.all()
    vendor=Vendor.objects.all()
    user=User.objects.all()
    return render(request,"add_order.html",{'product':product,'vendor':vendor,'user':user}) 

def searchorder(request):
    rp=orderproducts.objects.all()
    if request.method == 'POST':
        deliverydate=request.POST.get('deliverydate')
        vendorname=request.POST.get('vendorname')
        productname=request.POST.get('productname')
        customeremail=request.POST.get('customeremail')
        if deliverydate:
            k1=orderproducts.objects.filter(deliverydate=deliverydate)
        elif vendorname:
            k1=orderproducts.objects.filter(vendordetails__vend_name=vendorname)
        elif productname:
            k1=orderproducts.objects.filter(productdetails__product_name=productname)
        elif customeremail:
            k1=orderproducts.objects.filter(customerdetails__custemail=customeremail)
        else:
            k1=orderproducts.objects.filter(deliverydate=deliverydate,vendordetails__vend_name=vendorname,productdetails__product_name=productname,customerdetails__custemail=customeremail)
    return render(request,"ordersearch.html",{'k1':k1})     
    
    
def show_order(request):
    lc=Orders.objects.all()
    return render(request,"list_order.html",{'lc':lc})

def updateorder(request,orderid):
    k=Orders.objects.filter(id=orderid).values()
    if request.method == 'POST':
        o_num = request.POST.get('order_no')
        o_date = request.POST.get('order_date')
        total = request.POST.get('order_total')
        pname = request.POST.get('prod_name')
        r=Product.objects.get(product_name=pname)
        price = request.POST.get('prod_price')
        quant=request.POST.get('prod_quant') 
        c_email = request.POST.get('cust_email')
        cfk=User.objects.get(email=c_email)
        d_date = request.POST.get('delivery_date')
        ven_name = request.POST.get('v_name')
        vfk=Vendor.objects.get(Vendor_Name=ven_name)
        # d_status = request.POST.get('is_Delivered')
        # status=request.POST.get('status')
        k.update(Order_no=o_num,Order_date=o_date,Order_total=total,Product_name=r,Product_Price=price,Customer_email=cfk,Delivery_date=d_date,Vendor_Name=vfk)
        return redirect('showorder')
    product=Product.objects.all()
    vendor=Vendor.objects.all()
    user=User.objects.all()
    return render(request,"update_order.html",{'k':k[0],'id':orderid,'product':product,'vendor':vendor,'user':user}) 
          
 
def deleteorder(request,orderid):
   k=Orders.objects.get(id=orderid)
   k.delete()
   return redirect("showorder")  


    
    
