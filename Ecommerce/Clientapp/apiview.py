from django.shortcuts import render,redirect
from .models import User,UserManager,Order_Details,Orders
from Vendorapp.models import Vendor
from Productapp.models import Product,category,subcategory
from .serializers import UserSerializer,orderSerializer,orderdetailsSerializer
from django.contrib.auth import authenticate,login,logout

from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views  import ObtainAuthToken

from rest_framework.response import Response
 

#view for add prod ,list prod,add  n list cat,add n list subcat,add n list vendor ,add n list order,order search
#model for delivery boy n view for adding n listing driver

@api_view(['POST'])

def signup(request):
    if request.method == 'POST':
        username = request.data.get('username')
        email = request.data.get('email')
        role= request.data.get('role')
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')
        password = request.data.get('password')
        address = request.data.get('address') 
        postcode = request.data.get('postcode')
        city = request.data.get('city')
        phone = request.data.get('phone')
        Dob= request.data.get('Dob')
        root=User.objects.create_user(username=username,email=email,role=role,first_name=first_name,last_name=last_name,password=password,address=address,postcode=postcode,city=city,phone=phone,Dob=Dob)
        Token.objects.create(user=root)
        return Response(data = {'username':root.username,'response':'Successfully registered a user','token':Token.objects.get(user=root).key})

       
@api_view(['POST'])
def signin(request):
    if request.method == 'POST':
        mail=request.data.get('email')
        password=request.data.get('password')
        user = authenticate(request,email=mail,password=password)
        if user:
            login(request,user)
            return Response({'message':'success','email':user.email})
        else:
            return Response({'status':0})


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def showallcustomer(request):
    show = User.objects.all()
    serializer = UserSerializer(show,many=True)
    return Response(serializer.data)
    

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def updatecustomer(request,userid):
    u=User.objects.filter(id=userid).values()
    if request.method == 'POST':
        username = request.data.get('username')
        email = request.data.get('email')
        role= request.data.get('role')
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')
        address = request.data.get('address')
        postcode = request.data.get('postcode')
        city = request.data.get('city')
        phone = request.data.get('phone')
        Dob= request.data.get('Dob')
        User.objects.update(username=username,email=email,role=role,first_name=first_name,last_name=last_name,address=address,postcode=postcode,city=city,phone=phone,Dob=Dob)
        return Response({'message':'updated successfully'})


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def deletecustomer(request,userid):
    k=User.objects.get(id=userid)
    k.delete()
    return Response("Customer Removed")

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def searchcustomer(request):
    su = User.objects.all()
    if request.method == 'POST':
        UserName=request.data.get('username')
        d=User.objects.filter(username=UserName)
        return Response(data={'username':d.username ,'response':"Customer Removed"})
    serializer = UserSerializer(su,many=True)
    return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_order(request): 
    if request.method == 'POST':
        order_no = request.data.get('Order_no')
        order_date = request.data.get('Order_date')
        order_total= request.data.get('Order_total')
        cust_email = request.data.get('Customer_email')
        cemail=User.objects.get(email=cust_email)
        delivery_date = request.data.get('Delivery_date')
        prod_name=request.data.get('Product_name')
        pname=Product.objects.get(product_name=prod_name)
        prod_price=request.data.get('Product_Price')
        price=Product.objects.get(price=prod_price)
        prod_quantity=request.data.get('Quantity')      #stock-quantity
        sub_total = int(prod_quantity)*int(prod_price)
        total_price=request.data.get(Total_price=sub_total)
        delivery_status = request.data.get('is_Delivered')
        vend_name = request.data.get('Vendor_Name') 
        vname=Vendor.objects.get(Vendor_Name=vend_name)
        Orders.objects.create(Order_no=order_no,Order_date=order_date,Order_total=order_total,Customer_email=cemail,Delivery_date=delivery_date,is_Delivered=delivery_status,Vendor_name=vname,Total_price=total_price,Product_name=pname,Product_Price=price)
        return Response({'message':'order added successfully'})



@api_view(['GET'])
@permission_classes([IsAuthenticated])
@permission_classes([IsAuthenticated])
def show_order(request):
    show = Orders.objects.all()
    serializer = orderSerializer(show,many=True)
    return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_orderDetails(request):
    if request.method=="POST":
        order_no=request.data.get('Order_no')
        prod_name=request.data.get('Productname')       
        pname=Orders.objects.get(Product_name=prod_name)
        prod_quantity=request.data.get('product_quantity')
        pquantity=Orders.objects.get(product_quantity=prod_quantity)
        prod_price=request.data.get('Product_Price')
        pprice=Orders.objects.get(Product_Price=prod_price)
        sub_total = request.data.get('subtotal')
        payment_method = request.data.get('payment_method')
        payment_status = request.data.get('payment_status')
        Order_Details.objects.create(Order_no=order_no,Order_date=order_date,Product_name=pname,Product_Price=pprice,product_quantity=pquantity,Subtotal=sub_total,Payment_method=payment_method,Payment_Status=payment_status,Customer_email=cust_email,Vendor_name=V_name)
        return Response({'message':'success'})


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def show_orderdetails(request):
    show = Order_Details.objects.all()
    serializer = orderdetailsSerializer(show,many=True)
    return Response(serializer.data)
