from django.contrib import admin
from .models import User,Orders,Order_Details
# Register your models here.
admin.site.register(User)
admin.site.register(Orders)
admin.site.register(Order_Details)