"""Ecommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Clientapp import views,apiview
from rest_framework.authtoken.views  import obtain_auth_token
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index,name='index'),
    path('Home',views.home,name='home'),
    path('Shop',views.shop,name='shop'),
    path('single',views.single,name='single'),
    # path('Home',views.home,name='home'),
    # path('Home',views.home,name='home'),

    path('register',views.signup,name='reg'), 
    path('signup',apiview.signup,name='signup'),
    path('log',views.Signin,name='login'),
    path('logout',views.Signout,name='logout'),
           #CUSTOMER
    path('customer_list',views.Listcustomer,name='listall'),
    path('show_cust',apiview.showallcustomer,name='showall'),

    path('Search_all',views.searchcustomer,name='search'),
    path('del/<int:userid>',views.deletecustomer,name='del'),
    path('u<int:userid>',views.updatecustomer,name='up'),
           #ORDERS
    path('add_order',views.add_order,name='addorder'),
    path('show_order',views.show_order,name='showorder'),
    path('orderlist',apiview.show_order,name='orderlist'),

    path('delete_order/<int:orderid>',views.deleteorder,name='del_order'),
    path('update_order<int:orderid>',views.updateorder,name='up_order'),
               #ORDER_DETAILS
    # path('add_orderdetails',views.add_orderDetails,name='addorderdetails'),
    # path('show_orderdetails',views.show_orderdetails,name='showorderdetails'),
    # path('delete_od/<int:orderid>',views.delete_od,name='del_od'),
    # path('update_od/<int:orderid>',views.update_od,name='up_od'),
  
    
]
