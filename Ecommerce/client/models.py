from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.contrib.auth.hashers import make_password
from vendor.models import vendor
from product.models import productmodel

# Create your models here.
class UserManager(BaseUserManager):
    def _create_user(self, email, password, **other_fields):

        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not email:
            raise ValueError('An Email address must be set')
        email = self.normalize_email(email)

        user = self.model(email=email, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **other_fields)

    def create_superuser(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **other_fields)

class User(AbstractUser):
    # ROLE = (
    #     ('admin','admin'),
    #     ('customer','customer'),
    #     ('driver','driver')
    # )
    username=models.CharField(max_length=100,null=True,blank=True)
    customer_id=models.IntegerField(null=True)
    email=models.EmailField(max_length=100,null=True,blank=True,unique=True)
    created_at=models.DateField(null=True)
    first_name=models.CharField(max_length=100,null=True,blank=True)
    last_name=models.CharField(max_length=100,null=True,blank=True)
    password=models.CharField(max_length=100,null=True,blank=True)
    address=models.CharField(max_length=100,null=True,blank=True)
    postcode=models.CharField(max_length=100,null=True,blank=True)
    city=models.CharField(max_length=100,null=True,blank=True)
    phone=models.IntegerField(null=True)
    # role=models.CharField(max_length=100,null=True,blank=True,choices=ROLE)
    # image=models.ImageField(upload_to='customer',blank=True,null=True)

    
    

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['address','postcode','city','phone']
    objects = UserManager()

    def get_username(self):
        return self.email
class orders(models.Model):
    STATUS=(
        ('Pending','Pending'),
        ('Completed','Completed'),
        ('Ordered','Ordered'),
        ('Accepted','Accepted'),
        ('Out For Delivery','Out For Delivery'),
        ('Order Cancel','Order Cancel'),
        ('Customer Cancel','Customer Cancel'),
        ('Delivered','Delivered'),
        ('Add to cart','add to Cart'),
        ('Assigned to driver', 'Assigned to Driver')

    )
    order_no=models.IntegerField(null=True)
    order_date=models.DateField(null=True)
    order_total=models.IntegerField(null=True)
    customer=models.ForeignKey(User,on_delete=models.CASCADE,null=True)
    delivery_date=models.DateField(null=True)
    is_delivered=models.CharField(max_length=50,null=True)
    status=models.CharField(max_length=50,null=True,choices=STATUS)
    vendor=models.ForeignKey(vendor,on_delete=models.CASCADE,null=True)
    def get_date(self):
        self.delivery_date.date()

class orderdetails(models.Model):
    product=models.ForeignKey(productmodel,on_delete=models.CASCADE,null=True)
    product_quantity=models.IntegerField(null=True)
    product_price=models.IntegerField(null=True)
    order=models.ForeignKey(orders,on_delete=models.CASCADE,null=True)
    subtotal=models.IntegerField(null=True)
    payment_method=models.CharField(max_length=50,null=True)
    payment_status=models.CharField(max_length=50,null=True)
    def __str__(self):
        return self.payment_status

class driverdetails(models.Model):
    driver_name=models.CharField(max_length=50,null=True)
    driver_age=models.IntegerField(null=True)
    place=models.CharField(max_length=50,null=True)
    experience=models.IntegerField(null=True)
    def __str__(self):
        return self.driver_name
    
