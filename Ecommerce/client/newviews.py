from .models import *
from django.shortcuts import render
from django.http import HttpResponse
from pathlib import Path
import os
from django.contrib.auth import authenticate,login,logout
from django.core.files.storage import FileSystemStorage

def register(request):
    if request.method == 'POST':
        uname = request.POST.get('username')
        email = request.POST.get('email')
        pwd=request.POST.get('password')
        User.objects.create_user(username=uname,email=email,password=pwd)
        return render(request,"signin.html")
    return render(request,"signup.html")

def userlogin(request):
    if request.method=='POST':
        email=request.POST.get('email')
        password=request.POST.get('password') 
        user = authenticate(request,email=email,password=password)
        if user:
            login(request,user)
            return render(request,"index.html")
        else:
            return HttpResponse(' Wrong Email And Password')
    return render(request,"signin.html")

def drivers(request):
    
    if request.method == 'POST':
        name=request.POST.get('drivername')
        age=request.POST.get('driverage')
        place=request.POST.get('driverplace')
        exp=request.POST.get('experience')
        driverdetails.objects.create(driver_name=name,driver_age=age,place=place,experience=exp)
        
        
    dr=driverdetails.objects.all()
    return render(request, "driverlist.html", {'dr':dr})

def AddUser(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        c_id = request.POST.get('customer_id')
        email = request.POST.get('email')
        created_at = request.POST.get('created_at')
        fname = request.POST.get('first_name')
        lname = request.POST.get('last_name')
        pwd = request.POST.get('password')
        address = request.POST.get('address')
        p_code = request.POST.get('postcode')
        city = request.POST.get('city')
        phone = request.POST.get('phone')
        roles=request.POST.get('role')
        image=request.FILES['image']
        c=FileSystemStorage()
        fname=c.save(image.name,image)

        User.objects.create_user(username=username,customer_id=c_id,email=email,created_at=created_at,first_name=fname,last_name=lname,password=pwd,address=address,postcode=p_code,city=city,phone=phone,role=roles,image=fname)
    u=User.objects.all()
    return render(request, "user.html", {'u':u})

def userlogout(request):
    logout(request)
    return render(request,"signin.html")

def driversearch(request):
    if request.method=='POST':
        dname=request.POST.get('dname')
        dr=driverdetails.objects.filter(driver_name=dname)
    return render(request, "driverlist.html", {'dr':dr})
def usersearch(request):
    if request.method=='POST':
        uname=request.POST.get('username')
        u=User.objects.filter(username=uname)

    return render(request, "user.html", {'u':u})