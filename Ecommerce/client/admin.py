from django.contrib import admin
from .models import User,orderdetails,orders,driverdetails


# Register your models here.
admin.site.register(User)
admin.site.register(orders)
admin.site.register(orderdetails)
admin.site.register(driverdetails)
