from django.contrib import admin
from django.urls import path
from client import views,newviews,html
from rest_framework.authtoken.views import obtain_auth_token


urlpatterns = [
    path('admin/', admin.site.urls),
    path('addcustomer',views.AddCustomer,name='add'),
    path('allcustomers',views.AllCustomers,name='allcustomers'),
    path('login',views.CustomerLogin,name='login'),
    path('updatecustomer<int:userid>',views.UpdateCustomer,name='update'),
    path('home',views.home,name='home'),
    path('order',views.CreateOrder,name='order'),
    path('showorders',views.orderdetail,name='orders'),
    path('getuser<int:userid>',views.GetUser,name='getuser'),
    path('date',views.datefilter,name='date'),

    # front-end urls
    path('register',newviews.register,name='register'),
    path('userlogin',newviews.userlogin,name='userlogin'),
    path('driver',newviews.drivers,name='driver'),
    path('adduser',newviews.AddUser,name='adduser'),
    path('logout',newviews.userlogout,name='logout'),
    path('dsearch',newviews.driversearch,name='dsearch'),
    path('usearch',newviews.usersearch,name='usearch'),




    
    # front end urls
    path('hf',html.hf,name='hf'),
    path('about',html.about,name='about'),
    path('reveal',html.reveal,name='reveal'),
    path('blog',html.blog,name='blog'),
    path('contact',html.contact,name='contact'),
    path('checkout',html.checkout,name='checkout'),
    path('delivery',html.delivery,name='delivery'),
    path('login',html.login,name='login'),
    path('payment',html.payment,name='payment'),
    path('reciept',html.reciept,name='reciept'),
    path('product',html.product,name='product'),
    path('pgrid',html.pgrid,name='pgrid'),
    path('plist',html.plist,name='plist'),
    path('ptopbar',html.ptopbar,name='ptopbar'),
    path('cprofile',html.cprofile,name='cprofile'),
    path('cpnotifications',html.cpnotifications,name='cpnotifications'),
    path('cporders',html.cporders,name='cporders'),
    path('cprepass',html.cprepass,name='cprepass'),
    path('cpwishlist',html.cpwishlist,name='cpwishlist'),
    path('services',html.services,name='services'),


]