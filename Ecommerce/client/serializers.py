from rest_framework import serializers
from .models import *



class orderdetailsserializer(serializers.ModelSerializer):
    class Meta:
        model = orderdetails
        fields = ['product','product_quantity','subtotal']

class orderserializer(serializers.ModelSerializer):
    product=orderdetailsserializer(source='orderdetails_set',many=True)
    
    class Meta:
        model = orders
        fields = ['order_no','order_date','delivery_date','product']

class usersserializer(serializers.ModelSerializer):
    order=orderserializer(source='orders_set',many=True)
    class Meta:
        model = User
        fields = ['username','email','order']

