from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.http import HttpResponse
from .models import *
from vendor.models import *
from .serializers import usersserializer
from django.contrib.auth import authenticate,login,logout
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from .serializers import orderdetailsserializer,orderserializer
from django.core.files.storage import FileSystemStorage


@api_view(['GET','POST'])
def AddCustomer(request):
    if request.method == 'POST':
        username = request.data.get('username')
        c_id = request.data.get('customer_id')
        email = request.data.get('email')
        created_at = request.data.get('created_at')
        fname = request.data.get('first_name')
        lname = request.data.get('last_name')
        pwd = request.data.get('password')
        address = request.data.get('address')
        p_code = request.data.get('postcode')
        city = request.data.get('city')
        phone = request.data.get('phone')
        roles=request.data.get('role')
        image=request.FILES['image']
        c=FileSystemStorage()
        fname=c.save(image.name,image)

        u = User.objects.create_user(username=username,customer_id=c_id,email=email,created_at=created_at,first_name=fname,last_name=lname,password=pwd,address=address,postcode=p_code,city=city,phone=phone,role=roles,image=fname)
        Token.objects.create(user=u)
        return Response(data = {'email':u.email,'response':'Successfully added a customer','token':Token.objects.get(user=u).key})
    
    return Response("Add New Customers")

@api_view(['POST'])
def CustomerLogin(request):
    if request.method=='POST':
        email=request.POST.get('email')
        password=request.POST.get('password') 
        user = authenticate(request,email=email,password=password)
        if user:
            token = Token.objects.get(user=user)
            login(request,user)
            return Response({'message':'successfully logined','token':token.key,'email':user.email})
        else:
            return Response({'status':0})


@api_view()
@permission_classes([IsAuthenticated])
def AllCustomers(request):
    values = User.objects.all()
    serializer = usersserializer(values,many=True)
    return Response(serializer.data)
    

@api_view(['GET','PUT'])
def UpdateCustomer(request,userid):
        details=User.objects.filter(id=userid).values()
    # if request.method == 'POST':
        username = request.data.get('username')
        c_id = request.data.get('customer_id')
        email = request.data.get('email')
        created_at = request.data.get('created_at')
        fname = request.data.get('first_name')
        lname = request.data.get('last_name')
        pwd = request.data.get('password')
        address = request.data.get('address')
        p_code = request.data.get('postcode')
        city = request.data.get('city')
        phone = request.data.get('phone')
        roles=request.data.get('role')
        image=request.FILES['image']
        c=FileSystemStorage()
        fname=c.save(image.name,image)
        
        details.update(username=username,customer_id=c_id,email=email,created_at=created_at,first_name=fname,last_name=lname,password=pwd,address=address,postcode=p_code,city=city,phone=phone,role=roles,image=fname)
        return Response(data = {'response':'Successfully updated a customer'})
        # return HttpResponse('Update Customer')


def home(request):
    return render(request,"index.html")


@api_view(['POST'])
def CreateOrder(request):
    if request.method == 'POST':
        pr=request.data.get('product')
        p = productmodel.objects.get(product_name=pr)
        
        
        pquantity=request.data.get('product_quantity')
        pprice=p.price
        subtotal=int(pquantity)*int(pprice)
        if p.stock<=0:
            return Response("Out Of Stock")
        else:
            p.stock-=int(pquantity)
            p.save()
            orderno=request.data.get('order_no')
            orderdate=request.data.get('order_date')
            ordertotal=request.data.get('order_total')
            deliverydate=request.data.get('delivery_date')
            isdelivered=request.data.get('is_delivered')
            paymentmethod=request.data.get('payment_method')
            paymentstatus=request.data.get('payment_status')

            ord=request.data.get('email')
            o=User.objects.get(email=ord)

            ven=request.data.get('vendor')
            v=vendor.objects.get(vendor_email=ven)

            a=orders.objects.create(order_no=orderno,order_date=orderdate,order_total=ordertotal,customer=o,delivery_date=deliverydate,is_delivered=isdelivered,vendor=v)
            orderdetails.objects.create(product=p,product_quantity=pquantity,product_price=pprice,subtotal=subtotal,order=a,payment_method=paymentmethod,payment_status=paymentstatus)
            return Response("order Placed")
    return Response("place order")

@api_view(['GET'])
def orderdetail(request):
    values = orders.objects.all()
    serializer = orderserializer(values,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def vendororder(request):
    values = vendor.objects.all()
    serializer = vendorserializer(values,many=True)
    return Response(serializer.data)



@api_view(['GET'])
def GetUser(request,userid):
        details=User.objects.filter(id=userid)
        serializer = usersserializer(details,many=True)
        return Response(serializer.data)

@api_view(['GET','POST'])
def datefilter(request):
    if request.method == "POST":
        date=request.data.get('order_date')
        values = orders.objects.filter(order_date=date)
        serializer = orderserializer(values,many=True)
        return Response(serializer.data)

        

    



