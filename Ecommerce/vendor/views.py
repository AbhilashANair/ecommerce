from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from .models import *
from django.http import HttpResponse
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from .serializers import *
# Create your views here.

@api_view(['GET','POST'])
@permission_classes([IsAuthenticated])

def addvendor(request):

    if request.method == 'POST':

        vendor_email = request.data.get('vendor_email')
        vendor_branch = request.data.get('vendor_branch')
        vendor_name = request.data.get('vendor_name') 
       
        vendor_password = request.data.get('vendor_password')
        vendor_address = request.data.get('vendor_address')
        vendor_phone = request.data.get('vendor_phone')
        

        vendor.objects.create(vendor_email=vendor_email,vendor_branch= vendor_branch,vendor_name= vendor_name,vendor_password=vendor_password,vendor_address=vendor_address,vendor_phone=vendor_phone)

    vuv=vendor.objects.all()
    vs=vendor_serial(vuv,many=True)
    return Response(vs.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])

def filtervendor(request,vendorid):

    vuv=vendor.objects.filter(id=vendorid).values()
    vs=vendor_serial(vuv,many=True)
    return Response(vs.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])

def listvendor(request):
    vuv=vendor.objects.all()
    vs=vendor_serial(vuv,many=True)
    return Response(vs.data)

@api_view(['GET','PUT'])
@permission_classes([IsAuthenticated])

def updatevendor(request,vendorid):

        obj=vendor.objects.filter(id=vendorid).values()

    #if request.method == 'POST':

        vendor_email = request.data.get('vendor_email')
        vendor_branch = request.data.get('vendor_branch')
        vendor_name = request.data.get('vendor_name') 
        vendor_password = request.data.get('vendor_password')
        vendor_address = request.data.get('vendor_address')
        vendor_phone = request.data.get('vendor_phone')
        
        obj.update(vendor_email=vendor_email,vendor_branch= vendor_branch,vendor_name= vendor_name,vendor_password=vendor_password,vendor_address=vendor_address,vendor_phone=vendor_phone)

        vr=vendor.objects.all()
        vu=vendor_serial(vr,many=True)
        return Response(vu.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])

def deletevendor(request,vendorid):

    dlt=vendor.objects.get(id=vendorid)
    dlt.delete()

    ve=vendor.objects.all()
    vd=vendor_serial(ve,many=True)
    return Response(vd.data)