from django.db import models
from rest_framework.authtoken.models import Token
# Create your models here.
class vendor(models.Model):
    
    vendor_email=models.EmailField(blank=True,null=True)
    vendor_branch=models.CharField(max_length=100,blank=True,null=True)
    vendor_name = models.CharField(max_length=100,blank=True,null=True)
    vendor_password =models.CharField(max_length=100,blank=True,null=True)
    vendor_address =models.CharField(max_length=100,blank=True,null=True)
    vendor_phone = models.IntegerField(blank=True,null=True)
   

    def __str__(self):
        return self.vendor_name
