from django.contrib import admin
from django.urls import path
from vendor import views,normal

urlpatterns =[
    path('admin/',admin.site.urls),
    path('addvendor',views.addvendor,name='addvendor'),
    path('listvendor',views.listvendor,name='listvendor'),
    path('filtervendor<int:vendorid>',views.filtervendor,name='filtervendor'),
    path('updatevendor<int:vendorid>',views.updatevendor,name='updatevendor'),
    path('deletevendor<int:vendorid>',views.deletevendor,name='deletevendor'),



    ################################### default veiws urls #####################################


    path('index',normal.index,name='index'),
    path('vadd',normal.vadd,name='vadd'),
    path('vlist',normal.vlist,name='vlist'),
    path('vfilter',normal.vfilter,name='vfilter'),
    path('vupdate<int:vendorid>',normal.vupdate,name='vupdate'),
    path('vdelete<int:vendorid>', normal.vdelete,name='vdelete'),



    
]


